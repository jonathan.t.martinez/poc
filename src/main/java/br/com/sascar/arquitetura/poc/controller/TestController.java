package br.com.sascar.arquitetura.poc.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/app/v1/sascar/poc")
public class TestController {
	
	@GetMapping
	public String getTest(String name) {
		return "Hi, " + name;
	}
	
	@GetMapping("/name")
	public String getTest2(String name) {
		return "Hi, asdfsadadsf " + name;
	}

}
