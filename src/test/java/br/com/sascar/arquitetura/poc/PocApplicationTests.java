package br.com.sascar.arquitetura.poc;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.sascar.arquitetura.poc.controller.TestController;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PocApplicationTests {
	
	@Autowired
	private TestController testController;

	@Test
	public void contextLoads() {
		
	}
	
	@Test
	public void getTest() {
		String result = testController.getTest("Jonathan");
		Assert.assertTrue(result != null);
	}
	
	@Test
	public void getTeste2() {
		String result = testController.getTest2("Jonathan");
		Assert.assertTrue(result != null);
	}

}
