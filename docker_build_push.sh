#!/bin/bash
organization=sascararquitetura
repo=sascar-poc-arquitetura
version_stable=stable
version_latest=latest

docker login -u sascararq -p Sascar@2019

if [ "$1" = "latest" ]
then
    docker build -t $organization/$repo:$version_latest .
    docker push $organization/$repo:$version_latest
elif [ "$1" = "stable" ]
then
    docker build -t $organization/$repo:$version_stable .
    docker push $organization/$repo:$version_stable
fi